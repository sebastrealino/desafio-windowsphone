﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ChallengeDribbble.Model;
using System.Windows.Media.Imaging;

namespace ChallengeDribbble.View
{
    public partial class PageDetails : PhoneApplicationPage
    {
        private App app;
        public PageDetails()
        {
            InitializeComponent();
            app = (Application.Current as App);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            EnableLoading(true);
            if (NavigationContext.QueryString.ContainsKey("id"))
            {
                string val = NavigationContext.QueryString["id"];
                app.DribbleReqApi.GetShotDetail(Convert.ToInt32(val), OnSuccessShotDetail, OnErrorShotDetail);
            }
        }

        private async void OnSuccessShotDetail(Shot obj)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                EnableLoading(false);
                Player player = obj.Player;
                BitmapImage imgAux = new BitmapImage(new Uri(obj.ImageURL));
                imgTopPlayer.Source = imgAux;
                imgAux = new BitmapImage(new Uri(player.avatar_url));
                imgMiddlePlayer.Source = imgAux;
                txtNamePlayer.Text = player.name;
                txtDescription.Text = obj.Description;
                txtTitle.Text = obj.Title;
                txtLikesCount.Text = obj.ViewsCount.ToString();
            });
        }

        private async void OnErrorShotDetail(Exception obj)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                EnableLoading(false);
                stcViewer.Visibility = Visibility.Collapsed;
                progressbar.IsIndeterminate = false;
                progressbar.Visibility = Visibility.Collapsed;
                //txtError.Visibility = Visibility.Visible;
                txtError.Text = "Não é possível mostrar os detalhes :'(";
                animateError.Begin();
            });
        }

        private void EnableLoading(bool isEnable)
        {
            if (isEnable)
            {
                stcViewer.Visibility = Visibility.Collapsed;
                progressbar.Visibility = Visibility.Visible;
                progressbar.IsIndeterminate = true;
            }
            else
            {
                stcViewer.Visibility = Visibility.Visible;
                progressbar.Visibility = Visibility.Collapsed;
                progressbar.IsIndeterminate = false;
            }

        }
    }
}