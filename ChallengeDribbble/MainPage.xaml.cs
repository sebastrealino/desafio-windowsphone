﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ChallengeDribbble.Resources;
using System.Collections.ObjectModel;
using ChallengeDribbble.Model;
using ChallengeDribbble.Control;
using Windows.UI.Core;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using System.Diagnostics;
using ChallengeDribbble.View;

namespace ChallengeDribbble
{
    public sealed partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        private App app;

        private ObservableCollection<Shot> shotsList;

        private bool RequestBeingLoaded = false;
        private int PageNumber = 1;
        private static int RequestPerPage = 15;

        public MainPage()
        {
            InitializeComponent();

            app = (Application.Current as App);

            shotsList = new ObservableCollection<Shot>();
            resultListBox.ItemRealized += resultListBox_ItemRealized;
            IsolatedStorageFile storageFolder = IsolatedStorageFile.GetUserStoreForApplication();
        }

        void resultListBox_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            
            if (!RequestBeingLoaded && resultListBox.ItemsSource != null && resultListBox.ItemsSource.Count >= RequestPerPage)
            {
                if (e.ItemKind == LongListSelectorItemKind.Item)
                {
                    if ((e.Container.Content as Shot).Equals(resultListBox.ItemsSource[resultListBox.ItemsSource.Count - RequestPerPage]))
                    {
                        Debug.WriteLine("Searching for {0}", PageNumber);
                        LoadDataFromSource();
                    }
                }
            }
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (app.DribbleReqApi == null)
            {
                LoadDataFromSource();
            }
        }

        private void LoadDataFromSource()
        {
            RequestBeingLoaded = true;
            app.DribbleReqApi = new DribbleReqApi();
            app.DribbleReqApi.GetShotsByPopular(OnShotsSuccess, OnShotsError, "page=" + PageNumber);
            PageNumber++;
        }

        private async void OnShotsError(Exception obj)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                resultListBox.Visibility = Visibility.Collapsed;
                txtError.Text = "Não é possível mostrar os detalhes :'(";
                animateError.Begin(); 
            });
        }

        private async void OnShotsSuccess(List<Shot> obj)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                resultListBox.Visibility = Visibility.Visible;
                RequestBeingLoaded = false;
                foreach (var shot in obj)
                {
                    shotsList.Add(shot);
                }
                resultListBox.ItemsSource = shotsList;
            });
        }
        private async void tap_event(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            UCShot shotControl = sender as UCShot;
            Dispatcher.BeginInvoke(() =>
            {
                this.NavigationService.Navigate(new Uri("/View/PageDetails.xaml?id=" + shotControl.Tag, UriKind.Relative));
            });
        }
 
    }
}