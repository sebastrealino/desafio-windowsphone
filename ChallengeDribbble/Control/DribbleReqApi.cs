﻿using ChallengeDribbble.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeDribbble.Control
{
    public class DribbleReqApi
    {
        private const string ConsBaseAdress = "http://api.dribbble.com";
        private const string ConsShots = "shots";
        private const string ConsPopular = "popular";

        private Requests concreteRequests;

        public DribbleReqApi()
        {
            this.concreteRequests = new Requests();
        }

        public void Dispose()
        {
            concreteRequests.DisposeHttpClient();
        }

        public void GetShots(Action<RootObject> success, Action<Exception> error = null, string parameters = "")
        {
            Uri uri = new Uri(ConsBaseAdress + "/" + ConsShots + "?" + parameters);
            this.concreteRequests.Get<RootObject>(uri, success, error);
        }

        public void GetShotsByPopular(Action<List<Shot>> success, Action<Exception> error = null, string parameters = "")
        {
            Uri uri = new Uri(ConsBaseAdress + "/" + ConsShots + "/" + ConsPopular + "?" + parameters);
            this.concreteRequests.Get<RootObject>(
                uri,
                (rootObject) =>
                {
                    if (rootObject != null && rootObject.shots.Count > 0)
                    {
                        success(rootObject.shots);
                    }
                },
                error);
        }

        public void GetShotDetail(int shotId, Action<Shot> success, Action<Exception> error = null, string parameters= "")
        {
            Uri uri = new Uri(ConsBaseAdress + "/" + ConsShots + "/" + shotId.ToString() + "?" + parameters);
            this.concreteRequests.Get<Shot>(
                uri,
                (shot) =>
                {
                    if (shot != null)
                    {
                        success(shot);
                    }
                },
                error);
        }

    }
}
