﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeDribbble.Control
{
    public class Requests
    {
        private HttpClient HttpClient { get; set; }

        public Requests()
        {
            CreateHttpClient();
        }

        private void CreateHttpClient()
        {
            if (HttpClient != null)
            {
                HttpClient.Dispose();
            }
            HttpClient = new HttpClient();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void DisposeHttpClient()
        {
            if (HttpClient != null)
            {
                HttpClient.Dispose();
            }
        }

        public void Get<T>(Uri uri, Action<T> successAction, Action<Exception> errorAction)
        {
            try
            {
                var message = HttpClient.GetAsync(uri, HttpCompletionOption.ResponseContentRead);
                message.ContinueWith((t) =>
                {
                    using (Stream stream = t.Result.Content.ReadAsStreamAsync().Result)
                    using (StreamReader sr = new StreamReader(stream))
                    using (JsonReader reader = new JsonTextReader(sr))
                    {
                        System.Diagnostics.Debug.WriteLine("Requests::Get {0}", uri.ToString());
                        JsonSerializer serializer = new JsonSerializer();
                        T obj = serializer.Deserialize<T>(reader);
                        if (successAction != null && obj != null)
                        {
                            successAction(obj);
                        }
                        else
                        {
                            NullReferenceException e = new NullReferenceException("Deserialized object is null");
                            System.Diagnostics.Debug.WriteLine(e.ToString());
                            if (errorAction != null)
                            {
                                errorAction(e);
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                if (errorAction != null)
                {
                    errorAction(e);
                }
            }
        }
    }
}
