﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeDribbble.Model
{
    public class RootObject
    {
        public string page { get; set; }
        public int per_page { get; set; }
        public int pages { get; set; }
        public int total { get; set; }
        public List<Shot> shots { get; set; }
    }
}