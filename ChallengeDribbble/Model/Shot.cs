﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChallengeDribbble.Model
{
    [DataContract]
    public class Shot
    {
        private string _description;
        [DataMember(Name = "description")]
        public string Description
        {
            get
            {
                if (_description != null)
                {
                    _description = Regex.Replace(_description, @"<[^>]+>|&nbsp;", "").Trim();
                }

                return _description;
            }
            set
            {
                _description = value;
            }
        }
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "title")]
        public string Title { get; set; }
        public int height { get; set; }
        public int width { get; set; }
        [DataMember(Name = "likes_count")]
        public int LikesCount { get; set; }
        [DataMember(Name = "comments_count")]
        public int CommentsCount { get; set; }
        public int rebounds_count { get; set; }
        public string url { get; set; }
        public string short_url { get; set; }
        [DataMember(Name = "views_count")]
        public int ViewsCount { get; set; }
        public int? rebound_source_id { get; set; }
        [DataMember(Name = "image_url")]
        public string ImageURL { get; set; }
        public string image_teaser_url { get; set; }
        [DataMember(Name = "image_400_url")]
        public string Image400URL { get; set; }
        [DataMember(Name = "player")]
        public Player Player { get; set; }
        [DataMember(Name = "created_at")]
        public string CreatedAt { get; set; }
    }
}